---
- name: Mise à jour du système
  apt:
    update_cache: yes
    upgrade: yes
    state: present

- name: Installation des paquets requis
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - apache2
    - php
    - mariadb-server
    - python3-mysqldb
    - php-xml
    - php-common
    - php-json
    - php-mysql
    - php-mbstring
    - php-curl
    - php-gd
    - php-intl
    - php-zip
    - php-bz2
    - php-imap
    - php-apcu
    - php-ldap
    - php8.2-fpm
    - gnupg2
    - curl
    - wget

- name: Création de la base de données et de l'utilisateur
  mysql_db:
    name: db_glpi
    state: present

- name: Attribution des privilèges à l'utilisateur
  mysql_user:
    name: glpi
    host: "{{ item }}"
    password: glpi
    priv: "db_glpi.*:ALL"
    state: present
  loop:
    - "localhost"
    - "127.0.0.1"

- name: Téléchargement et extraction de GLPI
  get_url:
    url: "https://github.com/glpi-project/glpi/releases/download/10.0.12/glpi-10.0.12.tgz"
    dest: "/tmp/glpi-10.0.12.tgz"

- name: Extraction de l'archive
  unarchive:
    src: "/tmp/glpi-10.0.12.tgz"
    dest: "/var/www/"
    remote_src: yes
    owner: www-data

- name: Création des répertoires
  file:
    path: "{{ item }}"
    state: directory
    owner: www-data
  loop:
    - "/etc/glpi"
    - "/var/lib/glpi"
    - "/var/log/glpi"

- name: Déplacement des fichiers de configuration
  command: mv /var/www/glpi/config /etc/glpi
  args:
    creates: /etc/glpi/config

- name: Déplacement des fichiers de données
  command: mv /var/www/glpi/files /var/lib/glpi
  args:
    creates: /var/lib/glpi/files

- name: Création du fichier downstream.php
  copy:
    content: |
      <?php
      define('GLPI_CONFIG_DIR', '/etc/glpi/');
      if (file_exists(GLPI_CONFIG_DIR . '/local_define.php')) {
          require_once GLPI_CONFIG_DIR . '/local_define.php';
      }
    dest: /var/www/glpi/inc/downstream.php
    owner: www-data

- name: Création du fichier local_define.php
  copy:
    content: |
      <?php
      define('GLPI_VAR_DIR', '/var/lib/glpi/files');
      define('GLPI_LOG_DIR', '/var/log/glpi');
    dest: /etc/glpi/local_define.php
    owner: www-data

- name: Configuration du VirtualHost pour Apache
  copy:
    content: |
      <VirtualHost *:80>

          ServerName localhost
          DocumentRoot /var/www/glpi/public

          <Directory /var/www/glpi/public>
              Require all granted
              RewriteEngine On
              RewriteCond %{REQUEST_FILENAME} !-f
              RewriteRule ^(.*)$ index.php [QSA,L]
          </Directory>

          <FilesMatch \.php$>
              SetHandler 'proxy:unix:/run/php/php8.2-fpm.sock|fcgi://localhost/'
          </FilesMatch>

      </VirtualHost>
    dest: /etc/apache2/sites-available/glpi.conf

- name: Création du lien symbolique pour activer le site GLPI
  file:
    src: /etc/apache2/sites-available/glpi.conf
    dest: /etc/apache2/sites-enabled/glpi.conf
    state: link

- name: Activation des modules apache
  command: sudo a2enmod rewrite proxy_fcgi setenvif

- name: Activation du site GLPI avec a2ensite
  command: sudo a2ensite glpi.conf

- name: Désactivation du site par défaut
  command: a2dissite 000-default.conf

- name: Redémarrage du service Apache
  systemd:
    name: apache2
    state: restarted

- name: Activation de php8.2-fpm
  command: a2enconf php8.2-fpm
  notify: reload apache2

- meta: flush_handlers

- name: Configuration de session.cookie_httponly dans php.ini
  lineinfile:
    path: /etc/php/8.2/fpm/php.ini
    regexp: '^session\.cookie_httponly\s*='
    line: "session.cookie_httponly = on"
    state: present

- name: Redémarrage du service PHP-FPM 8.2
  systemd:
    name: php8.2-fpm
    state: restarted

- name: Redémarrage du service Apache
  systemd:
    name: apache2
    state: restarted

### TELEGRAF ###
- name: Copy depots telegraf
  copy:
    src: ../roles/glpi/files/depots.sh
    dest: /usr/local/bin/depots.sh
    owner: debian
    group: debian   
    mode: '0755'
  notify:
    - Run import depots

- meta: flush_handlers

- name: Install Telegraf
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - telegraf

- name: Add line to rsyslog.conf
  lineinfile:
    path: /etc/rsyslog.conf
    line: "*.* @@(o)127.0.0.1:6514;RSYSLOG_SyslogProtocol23Format"
    insertafter: EOF

- name: Restart rsyslog service
  service:
    name: rsyslog
    state: restarted

- name: Copy conf telegraf
  copy:
    src: ../roles/glpi/files/telegraf.conf
    dest: /etc/telegraf/telegraf.conf
    mode: '0644'

- name: Start Telegraf service
  service:
    name: telegraf
    state: started
    enabled: yes

- name: Enable services at boot
  systemd:
    name: "{{ item }}"
    enabled: yes
  loop:
    - telegraf
