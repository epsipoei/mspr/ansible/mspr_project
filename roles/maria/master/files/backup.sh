#!/bin/bash

# Définition des variables
DB_USER="admin"
DB_PASSWORD="iopiop"
DB_NAME="mspr"
BACKUP_DIR="/tmp/backup"
REMOTE_SERVER="16.170.240.18"
REMOTE_USER="backy"
REMOTE_DIR="/srv/backup"
DATE=$(date +%Y-%m-%d_%H-%M-%S)
BACKUP_FILE="$BACKUP_DIR/$DB_NAME-$DATE.sql.gz"

# Création du backup compressé
MYSQL_PWD="$DB_PASSWORD" mysqldump -u $DB_USER $DB_NAME | gzip > $BACKUP_FILE

# Vérification que le fichier de backup a été créé
if [ -f $BACKUP_FILE ]; then
    # Envoi du fichier de backup vers le serveur distant avec SCP
    scp $BACKUP_FILE $REMOTE_USER@$REMOTE_SERVER:$REMOTE_DIR

    # Vérification que le fichier a été correctement reçu sur le serveur distant
    if ssh $REMOTE_USER@$REMOTE_SERVER "[ -f $REMOTE_DIR/$(basename $BACKUP_FILE) ]"; then
        # Suppression du fichier de backup sur le serveur local
        rm $BACKUP_FILE
    fi
fi

