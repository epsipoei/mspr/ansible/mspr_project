#!/bin/bash

sudo influx << EOF
CREATE USER telegraf WITH PASSWORD 'Telgr@f';
CREATE DATABASE telegraf_bdd;
GRANT ALL ON telegraf_bdd TO telegraf;
CREATE RETENTION POLICY "trente_jours" ON telegraf_bdd DURATION 30d REPLICATION 1 DEFAULT;
EOF

sudo systemctl restart influxdb
