#!/bin/bash

# Ajout d'un nouvel utilisateur et définition du mot de passe
sudo adduser --disabled-password --gecos "" cyber_admin
echo 'cyber_admin:iopiop' | sudo chpasswd

# Assurez-vous que l'utilisateur dispose des privilèges sudo
sudo usermod -aG sudo cyber_admin

# Mise à jour des dépôts
sudo apt-get update

# Installation de Squid
sudo apt-get -y install squid

# Définir la disposition du clavier en français
sudo localectl set-keymap fr

# Sauvegarde du fichier de configuration par défaut
sudo cp /etc/squid/squid.conf /etc/squid/squid.conf.bak

# Configuration du fichier squid.conf pour load balancing
cat <<EOT | sudo tee /etc/squid/squid.conf
acl SSL_ports port 443
acl Safe_ports port 80       # http
acl Safe_ports port 443       # https

# Autoriser l'accès à des serveurs web spécifiques
acl package_update dstdomain .ubuntu.com
acl package_update dstdomain .debian.orgacl SSL_ports port 443
acl sv_web1 dst 192.168.56.20  # IP de votre serveur web 1
acl sv_web2 dst 192.168.56.21  # IP de votre serveur web 2
acl pfsense dst 192.168.56.253 # IP de votre serveur pfsense
acl haproxy dst 192.168.57.23  # IP de votre serveur reverse proxy
acl from_haproxy src 192.168.57.23  # IP haproxy
acl from_wan src 192.168.1.73  # IP Wan

# Autoriser l'accès aux heures spécifiques
acl work_hours time M T W T F 9:00-17:00

# Limiter le nombre de requêtes par IP
acl throttleUsers maxconn 3

# acl Safe_ports port 21       # ftp
# acl Safe_ports port 70       # gopher
# acl Safe_ports port 210       # wais
# acl Safe_ports port 1025-65535    # unregistered ports
# acl Safe_ports port 280       # http-mgmt
# acl Safe_ports port 488       # gss-http
# acl Safe_ports port 591       # filemaker
# acl Safe_ports port 777       # multiling http

acl CONNECT method CONNECT

http_access deny !Safe_ports
http_access deny CONNECT !SSL_ports
http_access allow localhost manager
http_access deny manager
http_access allow localhost

# Combinaison de l'ACL pour autoriser l'accès à vos serveurs web pendant les heures de travail
http_access allow sv_web1 work_hours
http_access allow sv_web2 work_hours
http_access allow pfsense work_hours
http_access allow haproxy work_hours
http_access allow from_haproxy
http_access allow from_wan
http_access deny throttleUsers

# Autoriser l'accès à Internet pour les mises à jour de packages uniquement
http_access allow package_update internal_servers
http_access allow localnet

http_access deny all

http_port 192.168.57.22:3128
coredump_dir /var/spool/squid
refresh_pattern ^ftp:       1440    20% 10080
refresh_pattern ^gopher:    1440 0% 1440
refresh_pattern -i (/cgi-bin/|\?) 0    0% 0
refresh_pattern (Release|Packages(.gz)*)$      0       20%     2880
refresh_pattern .       0    20% 4320
EOT

# Ajout des entrées au fichier /etc/hosts
sudo -- sh -c "echo '192.168.56.20 server1' >> /etc/hosts"
sudo -- sh -c "echo '192.168.56.21 server2' >> /etc/hosts"

# Redémarrage du service Squid
sudo systemctl restart squid

# Configuration Netplan
cat <<EOT >> /etc/netplan/01-network-manager-all.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s8:
      routes:
        - to: 0.0.0.0/0
          via: 192.168.57.253
EOT

# Configuration du DNS
cat <<EOL | sudo tee /etc/netplan/01-netcfg.yaml > /dev/null
network:
  version: 2
  ethernets:
    enp0s8:
      dhcp4: no
      addresses: [192.168.57.22/24]
      gateway4: 192.168.57.253
      nameservers:
          addresses: [1.1.1.1]
EOL

# Appliquer les modifications Netplan
sudo netplan apply

# Éteindre la machine virtuelle
sudo poweroff
