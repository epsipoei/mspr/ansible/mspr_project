#!/bin/bash

# Générer la clé si elle n'existe pas
sudo -u backy ssh-keygen -t ed25519 -b 4096 -N '' -f /home/backy/.ssh/id_ed25519