# mspr_project

Mise en place de telegraf, influxdb, grafana:

- <https://antoinelounis.com/informatique/supervision/installation-grafana-influxdb-telegraf-debian/>
- <https://grafana.com/grafana/dashboards/12433-syslog/>

Grafana dashboard id:

- rsyslog: 12433
- metric: 928

Teleport :

- docs: <https://goteleport.com/docs/>

Lors du deploiement de l'agent sur les vm suivre les etapes sur l'ui

- curl en mode insecure ajouter -k  
*exemple:*  

    ```bash
    sudo bash -c "$(curl -k -fsSL <https://192.168.56.32/scripts/481128545407f7bde55e058b6a3df13d/install-node.sh>)"
    ```

- run teleport avec self-signed cert

    ```bash
    sudo teleport start -c /etc/teleport.yaml --insecure
    ```
