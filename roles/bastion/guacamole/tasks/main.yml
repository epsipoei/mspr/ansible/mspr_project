---
- name: Mise à jour du système
  apt:
    update_cache: yes
    upgrade: yes
    state: present

- name: Installation des paquets requis
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - build-essential
    - libcairo2-dev
    - libjpeg62-turbo-dev
    - libpng-dev 
    - libtool-bin 
    - uuid-dev 
    - libossp-uuid-dev 
    - libavcodec-dev 
    - libavformat-dev 
    - libavutil-dev 
    - libswscale-dev 
    - freerdp2-dev 
    - libpango1.0-dev 
    - libssh2-1-dev 
    - libtelnet-dev 
    - libvncserver-dev 
    - libwebsockets-dev 
    - libpulse-dev 
    - libssl-dev 
    - libvorbis-dev 
    - libwebp-dev
    - wget
    - mariadb-server
    - python3-mysqldb
    # - libguac-client-ssh0 
    # - libguac-client-rdp0

### GUACAMOLE SERVER ###
- name: Télécharger le fichier Guacamole Server
  get_url:
    url: "https://downloads.apache.org/guacamole/1.5.3/source/guacamole-server-1.5.3.tar.gz"
    dest: "/tmp/guacamole-server-1.5.3.tar.gz"

- name: Extraire l'archive Guacamole Server
  ansible.builtin.unarchive:
    src: "/tmp/guacamole-server-1.5.3.tar.gz"
    dest: "/tmp/"
    remote_src: yes

- name: Exécuter la commande configure
  ansible.builtin.command:
    cmd: "./configure --with-init-dir=/etc/init.d"
    chdir: "/tmp/guacamole-server-1.5.3/"

- name: Exécuter make
  community.general.make:
    chdir: "/tmp/guacamole-server-1.5.3/"
  become: true

- name: Exécuter make install
  community.general.make:
    chdir: "/tmp/guacamole-server-1.5.3/"
    target: install
  become: true

- name: Exécuter ldconfig
  ansible.builtin.command:
    cmd: "sudo ldconfig"

- name: Reload systemd
  ansible.builtin.systemd:
    daemon_reload: yes

- name: Démarrer guacd
  ansible.builtin.service:
    name: guacd
    state: started
    enabled: yes

- name: Créer le répertoire Guacamole
  ansible.builtin.file:
    path: "/etc/guacamole/{{ item }}"
    state: directory
  loop:
    - extensions
    - lib

### GUACAMOLE CLIENT ###
- name: Installation des paquets requis
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - tomcat9 
    - tomcat9-admin 
    - tomcat9-common 
    - tomcat9-user

- name: Télécharger le fichier Guacamole Server
  get_url:
    url: "https://downloads.apache.org/guacamole/1.5.3/binary/guacamole-1.5.3.war"
    dest: "/tmp/guacamole-1.5.3.war"

- name: Déplacer le fichier Guacamole WAR vers le répertoire Tomcat
  ansible.builtin.command:
    cmd: "sudo mv /tmp/guacamole-1.5.3.war /var/lib/tomcat9/webapps/guacamole.war"

- name: Redémarrer les services Tomcat et guacd
  ansible.builtin.service:
    name: "{{ item }}"
    state: restarted
    enabled: yes
  loop:
    - tomcat9
    - guacd

### MARIADB ###
- name: Créer la base de données Guacamole
  ansible.builtin.mysql_db:
    name: guacadb
    state: present
  environment:
    MYSQL_PWD: 'P@ssword!'

- name: Créer un utilisateur pour Guacamole
  ansible.builtin.mysql_user:
    name: guaca_nachos
    host: localhost
    password: 'P@ssword!'
    priv: 'guacadb.*:SELECT,INSERT,UPDATE,DELETE'
    state: present
  environment:
    MYSQL_PWD: 'P@ssword!'

- name: Télécharger et extraire le module JDBC Guacamole
  ansible.builtin.get_url:
    url: "https://downloads.apache.org/guacamole/1.5.3/binary/guacamole-auth-jdbc-1.5.3.tar.gz"
    dest: "/tmp/guacamole-auth-jdbc-1.5.3.tar.gz"

- name: Extraire le module JDBC Guacamole
  ansible.builtin.unarchive:
    src: "/tmp/guacamole-auth-jdbc-1.5.3.tar.gz"
    dest: "/tmp/"
    remote_src: yes

- name: Copier le jar JDBC dans les extensions Guacamole
  ansible.builtin.copy:
    src: "/tmp/guacamole-auth-jdbc-1.5.3/mysql/guacamole-auth-jdbc-mysql-1.5.3.jar"
    dest: "/etc/guacamole/extensions/"
    remote_src: yes

- name: Télécharger MySQL Connector/J
  ansible.builtin.command:
    cmd: "wget -O /tmp/mysql-connector-j-8.3.0.tar.gz https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-8.3.0.tar.gz"

- name: Extraire MySQL Connector/J
  ansible.builtin.unarchive:
    src: "/tmp/mysql-connector-j-8.3.0.tar.gz"
    dest: "/tmp/"
    remote_src: yes

- name: Copier le fichier JAR JDBC dans le répertoire Guacamole lib
  ansible.builtin.copy:
    src: "/tmp/mysql-connector-j-8.3.0/mysql-connector-j-8.3.0.jar"
    dest: "/etc/guacamole/lib/"
    remote_src: yes

- name: Exécuter les scripts SQL pour initialiser la base de données Guacamole
  ansible.builtin.shell:
    cmd: "cat /tmp/guacamole-auth-jdbc-1.5.3/mysql/schema/*.sql | mysql -u root --password='' guacadb"
  args:
    executable: /bin/bash

- name: Créer le fichier guacamole.properties
  ansible.builtin.file:
    path: /etc/guacamole/guacamole.properties
    state: touch
    owner: root
    group: root
    mode: '0644'

- name: Editer le fichier guacamole.properties
  ansible.builtin.lineinfile:
    path: /etc/guacamole/guacamole.properties
    line: "{{ item }}"
    insertafter: EOF
  with_items:
    - "mysql-hostname: 127.0.0.1"
    - "mysql-port: 3306"
    - "mysql-database: guacadb"
    - "mysql-username: guaca_nachos"
    - "mysql-password: P@ssword!"

- name: Créer le fichier guacd.conf
  ansible.builtin.file:
    path: /etc/guacamole/guacd.conf
    state: touch
    owner: root
    group: root
    mode: '0644'

- name: Editer le fichier guacd.conf
  ansible.builtin.blockinfile:
    path: /etc/guacamole/guacd.conf
    block: |
      [server]
      bind_host = 0.0.0.0
      bind_port = 4822
    marker: "# {mark} ANSIBLE MANAGED BLOCK"
    insertafter: EOF

- name: Redémarrer les services Tomcat, guacd et mariadb
  ansible.builtin.service:
    name: "{{ item }}"
    state: restarted
  loop:
    - tomcat9
    - guacd
    - mariadb

### TELEGRAF ###
- name: Copy depots telegraf
  copy:
    src: ../roles/guacamole/files/depots.sh
    dest: /usr/local/bin/depots.sh
    owner: debian
    group: debian   
    mode: '0755'
  notify:
    - Run import depots

- meta: flush_handlers

- name: Install Telegraf
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - telegraf

- name: Add line to rsyslog.conf
  lineinfile:
    path: /etc/rsyslog.conf
    line: "*.* @@(o)127.0.0.1:6514;RSYSLOG_SyslogProtocol23Format"
    insertafter: EOF

- name: Restart rsyslog service
  service:
    name: rsyslog
    state: restarted

- name: Copy conf telegraf
  copy:
    src: ../roles/guacamole/files/telegraf.conf
    dest: /etc/telegraf/telegraf.conf
    mode: '0644'

- name: Start Telegraf service
  service:
    name: telegraf
    state: started
    enabled: yes

- name: Enable services at boot
  systemd:
    name: "{{ item }}"
    enabled: yes
  loop:
    - telegraf

### Copy conf mysql server to active log
- name: Copy 50-server.cnf
  copy:
    src: ../roles/guacamole/files/50-server.cnf
    dest: /etc/mysql/mariadb.conf.d/50-server.cnf
    owner: root
    group: root
    mode: '0644'

- name: Redémarrer le service MySQL
  service:
    name: mysql
    state: restarted

### Depots pour filebeat & metricbeat ###

- name: Import Elasticsearch GPG key
  apt_key:
    url: https://artifacts.elastic.co/GPG-KEY-elasticsearch

- name: Add Elastic APT repository
  apt_repository:
    repo: deb https://artifacts.elastic.co/packages/8.x/apt stable main
    state: present

- name: Update apt cache
  apt:
    update_cache: yes

### Install filebeat & metricbeat ###
- name: Installation des paquets requis
  apt:
    name: "{{ item }}"
    state: present
  loop:
    - filebeat
    - metricbeat

### Conf filebeat ###
- name: Copy filebeat.yml
  copy:
    src: ../roles/guacamole/files/filebeat.yml
    dest: /etc/filebeat/filebeat.yml
    owner: root
    group: root   
    mode: '0600'

- name: Copy module mysql.yml
  copy:
    src: ../roles/guacamole/files/filebeat_modules.d/mysql.yml
    dest: /etc/filebeat/modules.d/mysql.yml
    owner: root
    group: root   
    mode: '0644'

- name: Setup mysql module for Filebeat
  ansible.builtin.command: filebeat setup

### Conf metricbeat ###
- name: Copy metricbeat.yml
  copy:
    src: ../roles/guacamole/files/metricbeat.yml
    dest: /etc/metricbeat/metricbeat.yml
    owner: root
    group: root   
    mode: '0600'

- name: Copy module mysql.yml
  copy:
    src: ../roles/guacamole/files/metricbeat_modules.d/linux.yml
    dest: /etc/metricbeat/modules.d/linux.yml
    owner: root
    group: root   
    mode: '0644'

- name: Setup mysql module for Metricbeat
  ansible.builtin.command: metricbeat setup

- name: Enable services at boot
  systemd:
    name: "{{ item }}"
    enabled: yes
  loop:
    - filebeat
    - metricbeat

- name: Start Telegraf service
  service:
    name: "{{ item }}"
    state: started
    enabled: yes
  loop:
    - filebeat
    - metricbeat