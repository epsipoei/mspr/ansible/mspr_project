#!/bin/bash

# Démarrer le service MySQL
service mysql start

# Créer la base de données Zabbix et l'utilisateur de base de données
sudo mysql << EOF

DROP DATABASE IF EXISTS mspr;
CREATE DATABASE mspr;
USE mspr;


-- tables relatives à la sonde et à ses caractéristiques techniques

create table SONDE (
num_sonde INT UNSIGNED,
nom_sonde VARCHAR(20),
cpu_sonde DECIMAL(2,1) NOT NULL,
ram_sonde SMALLINT NOT NULL,
id_semaos INT UNSIGNED NOT NULL,
etat_sonde ENUM('connectee', 'deconnectee', 'indefini') DEFAULT 'indefini',

CONSTRAINT CK_PK_SONDE PRIMARY KEY (num_sonde),
CONSTRAINT CHK_num_sonde CHECK (length(num_sonde) <= 10)
-- FK : SEMAOS(id_semaos)
);

create table DISQUE_SEMA (
id_disque INT UNSIGNED AUTO_INCREMENT, 
num_sonde INT UNSIGNED NOT NULL, 
type_disque VARCHAR(20) NOT NULL,
taille_disque SMALLINT NOT NULL,

CONSTRAINT CK_PK_disque_sema PRIMARY KEY (id_disque)
-- FK : SONDE(num_sonde)
);

create table SEMAOS (
id_semaos INT UNSIGNED AUTO_INCREMENT,
version_semaos VARCHAR(10) NOT NULL,
date_creation_os date NOT NULL,

CONSTRAINT CK_PK_SEMAOS PRIMARY KEY (id_semaos)
);

create table SCRIPT (
id_script INT UNSIGNED AUTO_INCREMENT,
nom_script VARCHAR(50) NOT NULL,
version_script VARCHAR(10) ,

CONSTRAINT CK_PK_SCRIPT PRIMARY KEY (id_script)
);
create table SCRIPT_SONDE (
num_sonde INT UNSIGNED, 
id_script INT UNSIGNED,
date_install_script date NOT NULL, 

CONSTRAINT CK_PK_SCRIPT_SONDE PRIMARY KEY (num_sonde, id_script)
-- FK : SONDE(num_sonde), SCRIPT(id_script)
);


-- Tables relatives au client

create table CLIENT (
id_client INT UNSIGNED AUTO_INCREMENT,
SIRET BIGINT,
nom_client VARCHAR(50) NOT NULL,
adresse_client VARCHAR(50) NOT NULL,
prestataire INT UNSIGNED,

CONSTRAINT CK_PK_client PRIMARY KEY (id_client),
CONSTRAINT CHK_SIRET CHECK (length(SIRET) = 14)
-- FK : CLIENT(id_client)
);

create table CONTACT (
id_contact INT UNSIGNED AUTO_INCREMENT,
nom_contact VARCHAR(50) NOT NULL,
id_client INT UNSIGNED NOT NULL,
tel_contact INT,
mail_contact VARCHAR(50),

CONSTRAINT CK_PK_CONTACT PRIMARY KEY (id_contact)
-- FK : CLIENT(id_client)
);

create table LOCATION (
id_client INT UNSIGNED,
num_sonde INT UNSIGNED,
date_deb_loc DATE NOT NULL,
date_fin_loc DATE,
date_recup_sonde DATE DEFAULT NULL,

CONSTRAINT CK_CPK_LOCATION PRIMARY KEY (id_client, num_sonde),
CONSTRAINT CHK_fin_loc CHECK (date_fin_loc > date_deb_loc),
CONSTRAINT CHK_recup_sonde CHECK (date_recup_sonde > date_deb_loc)
-- FK : SONDE(num_sonde), CLIENT(id_client)
);


-- Tables réseau

create table LAN (
id_LAN INT UNSIGNED AUTO_INCREMENT,
adresse_reseau VARCHAR(18) NOT NULL, 
nom_reseau VARCHAR(50),
id_client INT UNSIGNED,

CONSTRAINT CK_PK_LAN PRIMARY KEY (id_LAN)
-- FK : CLIENT(id_client)
);

create table RESEAU_SONDE (
num_sonde INT UNSIGNED,
id_LAN INT UNSIGNED,
ip_defaut_sonde VARCHAR(18) NOT NULL,

CONSTRAINT CK_PK_RESEAU_SONDE PRIMARY KEY (num_sonde, id_LAN)
-- FK : SONDE(num_sonde), LAN(id_LAN)
);


create table COMMUNICATION_VPN (
id_vpn INT UNSIGNED AUTO_INCREMENT, 
num_sonde INT UNSIGNED, 
nom_tunnel VARCHAR(8),
adresse_client_VPN VARCHAR(18) NOT NULL,

CONSTRAINT CK_PK_COMMUNICATION_VPN PRIMARY KEY (id_vpn)
-- FK : SONDE(num_sonde)
);

-- Tables relatives aux opérations/incident

create table OPERATION_SONDE (
id_operation INT UNSIGNED AUTO_INCREMENT, 
num_sonde INT UNSIGNED, 
motif_operation ENUM('Dépannage ponctuel', 'Audit', 'Maintenance'),
type_operation ENUM('Redémarrage', 'Déploiement'),
id_employe INT UNSIGNED NOT NULL,
date_recup_sonde DATE DEFAULT NULL,
debut_operation TIMESTAMP DEFAULT NOW(),
fin_operation TIMESTAMP DEFAULT NULL,

CONSTRAINT CK_PK_OPERATION_SONDE PRIMARY KEY (id_operation),
-- FK : SONDE(num_sonde), EMPLOYE(id_employe)
CONSTRAINT CHK_fin_operation CHECK (fin_operation > debut_operation or fin_operation = NULL)
);

create table INCIDENT (
id_incident INT UNSIGNED AUTO_INCREMENT,
num_sonde INT UNSIGNED NOT NULL,
date_incident DATETIME NOT NULL,
id_operation INT UNSIGNED DEFAULT NULL,

CONSTRAINT CK_PK_INCIDENT PRIMARY KEY (id_incident)
-- FK : SONDE(num_sonde), OPERATION(id_operation)
);


create table EMPLOYE (
id_employe INT UNSIGNED AUTO_INCREMENT,
nom_employe VARCHAR(50) NOT NULL,
prenom_employe VARCHAR(50) NOT NULL,

CONSTRAINT CK_PK_EMPLOYE PRIMARY KEY (id_employe)
);

EOF
